[tool.poetry]
name = "slugify"
version = "0.1.0"
description = ""
authors = ["Patrick Vergain <patrick.vergain@id3.eu>"]
readme = "README.md"

[tool.poetry.dependencies]
python = "^3.11"
slugify = "^0.0.1"


[build-system]
requires = ["poetry-core"]
build-backend = "poetry.core.masonry.api"
