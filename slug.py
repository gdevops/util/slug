"""slug.py

- https://github.com/un33k/python-slugify


Exemple d'appel
==================

::

    python slug.py --text "un projet gitlab pour les issues"

::

    un-projet-gitlab-pour-les-issues

::

    python slug.py --text "un projet gitlab pour les Issues très graves"

::

    un-projet-gitlab-pour-les-issues-tres-graves

What does mean the term slugify ?

The term "slugify" refers to the process of taking a piece of text and
removing any characters that are not letters or numbers, and then
replacing any spaces with dashes or underscores.

This is often used when creating URLs or file names from a piece of text,
in order to make the resulting string more user-friendly and easier to
read.

For example, the text "Hello World!" could be slugified to "hello-world"
or "hello_world".


"""

import sys
from slugify import slugify


def get_slug(text: str):
    slug = slugify(text)
    print(slug)


if __name__ == "__main__":
    """Point d'entrée du script Python."""
    text = ""
    for i, argument in enumerate(sys.argv):
        if argument == "--text":
            text = sys.argv[i + 1]

    get_slug(text)
